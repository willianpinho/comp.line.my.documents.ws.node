﻿"use strict";
var express = require('express');
var router = express.Router();
var models = require('../models/rdb');

/* Authenticate User */
router.post('/login', function (req, res) {
    var parameters = {
        email: req.body.email,
        phone: req.body.phone,
    }

    var statement = {
        where: {
            email: req.body.email,
            phone: req.body.phone,
        }
    }

    console.log("Testando login");
    models.user.login(statement, parameters, function (user,error) {
        if (error) {
            return res.status(500).json(error);
        } else {
            if (user == null) {
                return res.status(204).errorUtil.jsonFromError(error, 'unable.doesnt.exist', 204)
            } else {
                return res.status(200).json(user);
            }
        }
    })
});

/* Retrieve all Users */
router.get('/', function (req, res) {
    models.user.getAll(function (data, error) {
        if (error) {
          return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.users', 500));
        }
        return res.status(200).json(data);
      });
});

/* Create New User */
router.post('/', function (req, res) {
    var currentDate = new Date();

    var parameters = {
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        imageUrl: req.body.imageUrl,
        createdAt: currentDate,
        updatedAt: currentDate
    }

    var statement = {
        where: {
            email: req.body.email
        }
    }

    
    models.user.insert(statement, parameters, function (user, error) {
        if (error) {
          return res.status(500).json({'title' : 'Erro ao Cadastrar', 'message' : capitalizeFirstLetter(error.errors[0]['path'])+ ' inválido!' , 'code' : 500});
        }
        return res.status(200).json(user);
    });
});

/* Retrieve User Id */
router.get('/:userId', function (req, res) {
    var parameters = {
        where: {
          id: req.params.userId
        }
    }
    models.user.get(parameters, function (user,error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.user.id', 500));
        }
        return res.status(200).json(user);
    })
});

/* Update User with Id */
router.put('/:userId', function (req, res) {
    var parameters = {
        where: {
            id: req.params.userId
          }
    }

    var updateFields = {};

    if (req.body.name) {
        updateFields['name'] = req.body.name;
    }

    if (req.body.email) {
        updateFields['email'] = req.body.email;
    }

    if (req.body.phone) {
        updateFields['phone'] = req.body.phone;
    }

    if (req.body.imageUrl) {
        updateFields['imageUrl'] = req.body.imageUrl;
    }

    updateFields['updatedAt'] = new Date();
    
    models.user.updateData(parameters, updateFields, function (user, error) {
        console.log(user);
        console.log(error);
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, 'unable.to.update.user', 400));
        }

        return res.status(200).json(user);
    })
});

/* Delete User with Id */
router.delete('/:userId', function (req, res) {
    var parameters = {
        where: {
            id: req.params.userId
        }
    }

    models.user.delete(parameters, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "can't delete user", 400));
        }
        return res.status(204).json(data);
    })    
});

/* Retrieve All Documents from User Id */
router.get('/:userId/documents', function (req, res) {    
    var parameters = {
        where: {
          userId: req.params.userId
        }
    }

    models.document.getAll(parameters, function (data,error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.documents.from.user', 500));
        }
        return res.status(200).json(data);
    })
});

/* Retrieve Save Document for User Id */
router.post('/:userId/documents', function (req, res) {    
    var currentDate = new Date();

    var parameters = {
        title: req.body.title,
        url: req.body.url,
        userId: req.params.userId,
        createdAt: currentDate,
        updatedAt: currentDate
    }

    models.document.insert(parameters, function (data,error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.documents.from.user', 500));
        }
        return res.status(200).json(data);
    })
});

/* Retrieve Document Id from User Id */
router.get('/:userId/documents/:documentId', function (req, res) {    
    var parameters = {
        where: {
            id: req.params.documentId,
            userId: req.params.userId,
        }
    }

    models.document.get(parameters, function (data,error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.documents.from.user', 500));
        }
        return res.status(200).json(data);
    })
});

/* Update Document Id From User Id */
router.put('/:userId/documents/:documentId', function (req, res) {    
    var parameters = {
        where: {
            id: req.params.documentId,
            userId: req.params.userId,
        }
    }

    var updateFields = {};

    if (req.body.title) {
        updateFields['title'] = req.body.title;
    }

    if (req.body.url) {
        updateFields['url'] = req.body.url;
    }

    updateFields['updatedAt'] = new Date();

    models.document.updateData(parameters, updateFields, function (user, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, 'unable.to.update.document', 400));
        }
        return res.status(200).json(user);
    })
});

/* Delete Document from User Id */
router.delete('/:userId/documents/:documentId', function (req, res) {
    var parameters = {
        where: {
            id: req.params.documentId,
            userId: req.params.userId,
        }
    }

    models.document.delete(parameters, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "can't delete document", 400));
        }
        return res.status(204).json(data);
    })    
});

router.post('list', function (req,res) { 
    app.
    blobService.listBlobs(container, function (error, blobs) {
        response.render('blobs', {
            error: error,
            container: container,
            blobs: blobs
        });
    });
});

module.exports = router;