"use strict";
module.exports = function (sequelize, Sequelize) {
    var User = sequelize.define('user', {
        id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: true,
            validate: {
                is: ["^[a-zA-Zà-úÀ-Ú ]+$"]
            }
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
            isEmail: true
            }
        },
        phone: {
            type: Sequelize.STRING,
            allowNull: true
        },
        imageUrl: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
            paranoid: true,
            tableName: 'Users',
        });

        User.associate = function (models) {
            User.hasMany(models.document);
        }

        User.getAll = function (callback) {
            User.findAll().then(function(users) {
                return callback(users, null);
            }).catch(function(error) {
                return callback(null, error);
            });
        }
        
        User.insert = function (statement, parameters, callback) {
            User.findOrCreate(statement).spread(function (user,error) {
                return callback(user, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }
        
        User.get = function (parameters, callback) {
            User.findOne(parameters)
            .then(function (user) {
                return callback(user, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        User.login = function (statement, parameters, callback) {
            User.findOne(statement)
            .then(function (user) {
                return callback(user, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        User.updateData = function (statement, parameters, callback) {
            User.findOne(statement)
            .then(function (user) {
                if (user) {
                    user.update(parameters)
                }
                return callback(user, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }
        
        User.delete = function(statement,callback){
            User.destroy(statement)
            .then(function(user){
                return callback(user, null);
            }).catch(function(error) {
                return callback(null, error);
            });
        }

    return User;
    };