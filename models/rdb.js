"use strict";

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var env       = process.env.NODE_ENV || 'development';
var config    = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

var sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  operatorsAliases: false,
  dialect: config.dialect,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  dialectOptions: {
    encrypt: config.encrypt
  },
  port: config.port,
  trustServerCertificate: config.trustServerCertificate,
  hostNameInCertificate: config.hostNameInCertificate,
  loginTimeout: config.loginTimeout
});

var db = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "rdb.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

sequelize.authenticate().then(function(err) {
  console.log('Connection has been established successfully.');
}).catch(function (err) {
  console.log('Unable to connect to the database:', err);
});

sequelize.sync(function(err){});

db.sequelize = sequelize;

module.exports = db;
