"use strict";

module.exports = function (sequelize, Sequelize) {
    var Document = sequelize.define('document', {
            id: {
                type: Sequelize.BIGINT,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: Sequelize.STRING,
                allowNull: true,
                validate: {
                    is: ["^[a-zA-Zà-úÀ-Ú ]+$"]
                }
            },
            url: {
                type: Sequelize.STRING,
                allowNull: true
            }
        }, {
            paranoid: true,
            tableName: 'Documents'
        });

        Document.associate = function (models) {
            Document.belongsTo(models.user);
        }

        Document.getAll = function (parameters, callback) {
            Document.findAll(parameters)
            .then(function (document) {
                return callback(document, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        Document.insert = function (parameters, callback) {
            Document
            .build(parameters)
            .save()
            .then(function (document) {
                return callback(document, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        Document.get = function (parameters, callback) {
            Document.findOne(parameters)
            .then(function (document) {
                return callback(document, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        Document.updateData = function (statement, parameters, callback) {
            Document.findOne(statement)
            .then(function (document) {
                if (document) {
                    document.update(parameters)
                }
                return callback(document, null);
            }).catch(function (error) {
                return callback(null, error);
            });
        }

        Document.delete = function(statement,callback){
            Document.destroy(statement)
            .then(function(document){
                return callback(document, null);
            }).catch(function(error) {
                return callback(null, error);
            });
        }
        
    return Document;
};
